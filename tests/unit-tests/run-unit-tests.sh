#!/usr/bin/bash

set -euxo pipefail

source /etc/os-release

# Move to the checked out git repo with the test plans
# this should be the root of the dist-git repo
cd "${TMT_TREE}"

# install all test dependencies
sudo dnf install -y rpmdevtools
sudo dnf builddep -y osbuild-composer.spec

# Move to the directory with sources
cd "${TMT_SOURCE_DIR}"

# Extract the Source0 basename without extension
SRC_DIR=$(spectool --source 0 osbuild-composer.spec | sed 's/.\+\(osbuild-composer-[0-9]\+\)\.tar\.gz/\1/')

# Move to the extracted sources directory (patches are applied by default)
cd "${SRC_DIR}"

GOFLAGS="-mod=vendor -tags=exclude_graphdriver_btrfs" go test ./...
